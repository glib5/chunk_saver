from concurrent.futures import ThreadPoolExecutor as ThreadPool
from pathlib import Path
from typing import NamedTuple

from pandas import DataFrame as DF


class _ChunkInfo(NamedTuple):
    start:int
    end:int
    filename:Path


def _prepare_chunkinfo(dflen:int, folder:Path, name:str, CHUNKSIZE:int=500_000) -> list[_ChunkInfo]:
    chunkinfos = []
    for i, start in enumerate(range(0, dflen, CHUNKSIZE), start=1):
        end = min((start + CHUNKSIZE, dflen))
        fname = folder / f"{i}_{name}"
        chunkinfos.append(_ChunkInfo(start, end, fname))
    return chunkinfos


def save_chunks_parallel(df:DF, folder:Path, name:str) -> None:
    """saves a pandas DataFrame to file in chunks -- using threads"""
    assert isinstance(df, DF)
    assert isinstance(folder, Path)
    assert isinstance(name, str)
    assert Path(name).suffix != ""
    # define the save worker here so the df variable is shared and not copied in each thread
    def save_chunk(info:_ChunkInfo):
        ext = info.filename.suffix
        chunk = df.iloc[info.start:info.end]
        if ext == ".csv":
            chunk.to_csv(info.filename, index=False)
        elif ext == ".pkl":
            chunk.to_pickle(info.filename)
        elif ext == ".parquet":
            chunk.to_parquet(info.filename)
        raise TypeError(f"Can't save to this file format: {info.filename.name}")
    chunkinfos = _prepare_chunkinfo(len(df), folder, name)
    with ThreadPool() as pool:
        pool.map(save_chunk, chunkinfos)
        