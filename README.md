# chunk_saver

convenience function to save a big file in chunks

``` python

from random import choices

from pandas import DataFrame as DF


N = 1<<21
df = DF()
df["id"] = tuple(range(1, 1+N))
df["grade"] = choices(["A", "B", "C", "D", "E", "F"], k=N)
folder = Path(__file__).parent
name = "test_par_save.parquet"

save_chunks_parallel(df, folder, name)

```